import React, { useState } from "react";
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";
import "./App.css";
import RestrictedArea from "./components/restricted-area/index";

function App() {
  const [allowed, setAllowed] = useState(false);
  const access = () => {
    setAllowed(true);
  };
  return (
    <Router>
      <div className="App">
        <header className="App-header">
          <h1>{allowed === true ? "Acesso concedido" : "Acesso negado"}</h1>
          <Link to="/">inicio</Link>

          <Link to="/restrict-area">Acesso restrito</Link>
          <button onClick={access}>Acessar</button>
          <Switch>
            <Route exact path="/">
              <h1>Bem vindo</h1>
              <h2>Entre</h2>
            </Route>
            <Route path="/restrict-area">
              <RestrictedArea allowed={allowed} />
            </Route>
          </Switch>
        </header>
      </div>
    </Router>
  );
}

export default App;
