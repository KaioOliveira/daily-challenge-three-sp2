import React from "react";
import { useHistory } from "react-router-dom";
const RestrictedArea = (props) => {
  let history = useHistory();
  if (props.allowed !== true) {
    history.push("/");
  }

  return (
    <div>
      {" "}
      <h1>Bem vindo</h1>
      <h2>Area Restrita</h2>
    </div>
  );
};
export default RestrictedArea;
